import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const state = {
    sidebarShow: null,
    sidebarMinimize: false,
    darkMode: 'c-dark-theme',
    lang: 'zhchs',
    token: null,
    user: null,
    root: null,
    IPPORT: '',
    WSROOT: '',
    title: '',
    VideoCodec: '',
    Resolution: '',
    room: '',
    pushaudio: '',
    pushvideo: '',
    bitrate: '',
    videowidth: '',
    videoheight: '',
    resolution1: '',
    flag: false,
    flag1: false
};

const mutations = {
    toggleSidebarDesktop(state) {
        const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow);
        state.sidebarShow = sidebarOpened ? false : 'responsive';
    },
    // toggleSidebarMobile (state) {
    //   const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
    //   state.sidebarShow = sidebarClosed ? true : 'responsive'
    // },
    setResolution1(state, resolution1) {
        state.resolution1 = resolution1
    },
    setFlag(state, flag) {
        state.flag = flag;
    },
    setFlag1(state, flag1) {
        state.flag1 = flag1;
    },
    setBitrate(state, bitrate) {
        state.bitrate = bitrate;
    },
    setWidth(state, width) {
        state.videowidth = width;
    },
    setHeight(state, height) {
        state.videoheight = height;
    },
    set(state, [variable, value]) {
        state[variable] = value;
    },
    toggle(state, variable) {
        state[variable] = !state[variable];
    }
};

export default new Vuex.Store({
    state,
    mutations
});